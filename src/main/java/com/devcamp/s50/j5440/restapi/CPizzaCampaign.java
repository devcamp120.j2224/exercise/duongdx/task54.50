package com.devcamp.s50.j5440.restapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CPizzaCampaign {
    @CrossOrigin
	@GetMapping("/devcamp-simple")
	public String simple() {
		return "test campaign";
	}
    @CrossOrigin
	@GetMapping("/devcamp-date")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello Piiza love ! Hôm nay %s , mua 1 tặng 1.", dtfVietnam.format(today));
	}
    @CrossOrigin
	@GetMapping("/devcamp-welcome")
	public String getLuckyNumber(@RequestParam(name = "username") String username
                                ,@RequestParam(name = "firstname") String firstname
                                ,@RequestParam(name = "lastname") String lastname) {
        int randomIntNum = 1 + (int)(Math.random()* ((10 - 1) + 1 ));
        return String.format("Xin Chào %s ! Số May Mắn Của bạn Hôm nay là %s.", username,randomIntNum);
	}
}
